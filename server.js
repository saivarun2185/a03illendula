var path = require("path")
var express = require("express")
var logger = require("morgan")
var bodyParser = require("body-parser") // simplifies access to request body


var app = express()  // make express app
var http = require('http').Server(app)  // inject app into the server
app.use(express.static(__dirname + '/Assets'))
// 1 set up the view engine
app.set("views", __dirname+'/Assets') // path to views
app.set("view engine", "ejs") // specify our view engine

// 2 manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up the logger
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// 4 handle valid GET requests
app.get("/", function (request, response) {
  response.sendFile(__dirname+"/Assets/Illendula_Saivarun.html")
})
app.get("/Illendula_Saivarun",function(request,response){
  response.sendFile(__dirname+"/Assets/Illendula_Saivarun.html")
})
app.get("/Illendula_Saivarun1",function(request,response){
  response.sendFile(__dirname+"/Assets/Illendula_Saivarun1.html")
})
app.get("/Illendula_Saivarun2",function(request,response){
  response.sendFile(__dirname+"/Assets/Illendula_Saivarun2.html")
})
app.get("/index", function (request, response) {
  response.render("index")
})
app.get("/new-entry", function (request, response) {
  response.render("new-entry")
})

// 5 handle valid POST request
app.post("/Illendula_Saivarun2", function(request, response){
var api_key = 'key-9c0b54128e505b17571f01ebddd2f356';
var domain = 'sandboxef1fcfc94be9405eab4ba35bf02d45b1.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
 
var data = {
  from: 'Sai Varun <postmaster@sandboxef1fcfc94be9405eab4ba35bf02d45b1.mailgun.org>',
  to: 'Saivarun.Illendula@gmail.com',
  subject: request.body.name,
  html: "<b>Questions: <b>" + request.body.question+"<b>  From: <b>" +request.body.mailid,
};

mailgun.messages().send(data, function (error, body) {
  console.log(body);

  if(!error)
    response.send("Mail sent!")
  else
    response.send("Mail not sent!")
  
});
})

app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  })

  response.redirect("index")  // where to go next? Let's go to the home page :)
 
})

// 6 respond with 404 if a bad URI is requested
app.use(function (request, response) {
  response.status(404).render("404")
})

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/')
})
